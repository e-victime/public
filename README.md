# Projet **E-Victime**

---

## Contenu :

Ce dépôt contient en sous modules la totalité du projet **E-Victime**.

---

## Instruction de récupération des sources :

Pour la récupération de la totalité du projet il suffit d'exécuter le script `update.sh` qui fera l'initialisation et/ou la mise à jour des sous-modules correspondant aux différents micro-services.

---

**Dépôt:** [https://gitlab.com/uf-web/public/](https://gitlab.com/uf-web/public/)\
**Groupe Gitlab:** [https://gitlab.com/uf-web/](https://gitlab.com/uf-web/)
