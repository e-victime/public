#!/bin/bash
echo -e "Mise à jour du dépôt"
git pull
echo -e "Mise à jour des liens des sous modules"
git submodule sync --recursive
echo -e "Mise à jour des sous modules"
git submodule update --remote --recursive --init
